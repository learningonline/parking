package com.nor.parking;

import android.app.Application;

import com.nor.parking.model.User;

public class App extends Application {
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
