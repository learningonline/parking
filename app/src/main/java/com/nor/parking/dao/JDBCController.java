package com.nor.parking.dao;

import android.os.StrictMode;

import com.nor.parking.model.BaseModel;
import com.nor.parking.model.TableInfo;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class JDBCController {
    private static Connection connection;
    private static Connection getConnection() {
        try {
            if (connection != null && !connection.isClosed()) return connection;
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            String sConnURL = "jdbc:jtds:sqlserver://192.168.1.105:1433;databaseName=PARKING;user=sa;password=1;";
            connection = DriverManager.getConnection(sConnURL);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return connection;
    }

    public static void closeConnection() {
        try {
            connection.close();
            connection = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean execute(String sql) {
        try {
            Statement statement = getConnection().createStatement();
            return statement.executeUpdate(sql) > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        } finally {
            closeConnection();
        }
    }

    public static <T extends BaseModel> ArrayList<T> getData(Class<T> clz, String sql) {
        ArrayList<T> data = new ArrayList<>();
        try {
            Statement statement = getConnection().createStatement();
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                T obj = clz.newInstance();
                Field[] fields = clz.getDeclaredFields();
                for (Field f : fields) {
                    f.setAccessible(true);
                    TableInfo info = f.getAnnotation(TableInfo.class);
                    if (info == null){
                        continue;
                    }
                    String value = rs.getString(info.fieldName());
                    mapValue(obj, value, f);
                }
                data.add(obj);
            }
        }catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            closeConnection();
        }
        return  data;
    }

    private static <T extends BaseModel> void mapValue(T t, String value,
                                                Field f) throws IllegalAccessException {
        String type = f.getType().getSimpleName();
        if (type.equalsIgnoreCase("int")){
            f.setInt(t, Integer.parseInt(value));
            return;
        }
        if (type.equalsIgnoreCase(Long.class.getSimpleName())){
            f.setLong(t, Long.parseLong(value));
            return;
        }
        if (type.equalsIgnoreCase(Boolean.class.getSimpleName())){
            if (value.equals("1")) {
                f.setBoolean(t, true);
                return;
            }
            f.setBoolean(t, Boolean.parseBoolean(value));
            return;
        }
        if (type.equalsIgnoreCase(Float.class.getSimpleName())){
            f.setFloat(t, Float.parseFloat(value));
            return;
        }
        if (type.equalsIgnoreCase(Double.class.getSimpleName())){
            f.setDouble(t, Double.parseDouble(value));
            return;
        }
        f.set(t, value);
    }

}
