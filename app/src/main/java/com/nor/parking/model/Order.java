package com.nor.parking.model;

public class Order extends BaseModel{
    @TableInfo(fieldName = "Id")
    private int id;
    @TableInfo(fieldName = "UserName")
    private String userName;
    @TableInfo(fieldName = "IdLocation")
    private int idLocation;
    @TableInfo(fieldName = "Time_In")
    private long timeIn;
    @TableInfo(fieldName = "Time_Out")
    private long timeOut;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getIdLocation() {
        return idLocation;
    }

    public void setIdLocation(int idLocation) {
        this.idLocation = idLocation;
    }

    public long getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(long timeIn) {
        this.timeIn = timeIn;
    }

    public long getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(long timeOut) {
        this.timeOut = timeOut;
    }
}
