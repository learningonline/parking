package com.nor.parking.model;

import java.util.ArrayList;

public class ParkingLocation extends BaseModel {
    @TableInfo(fieldName = "Id")
    private int id;
    @TableInfo(fieldName = "Name")
    private String name;

    private ArrayList<Order> orders;

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<Order> orders) {
        this.orders = orders;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
