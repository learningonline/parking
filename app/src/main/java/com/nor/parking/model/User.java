package com.nor.parking.model;

public class User extends BaseModel{
    @TableInfo(fieldName = "UserName")
    private String name;
    @TableInfo(fieldName = "Pass")
    private String password;
    @TableInfo(fieldName = "Balance")
    private int balance;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }
}
