package com.nor.parking.utils;

import java.text.SimpleDateFormat;

public class DateFormatUtils {
    public static String formatTime(long time) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        return format.format(time);
    }

    public static String formatDateTime(long time) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            return format.format(time);
        } catch (Exception ex) {
            return "";
        }
    }
}
