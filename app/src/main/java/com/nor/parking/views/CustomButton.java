package com.nor.parking.views;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;

import androidx.appcompat.widget.AppCompatButton;

import com.nor.parking.R;

public class CustomButton extends AppCompatButton {
    public CustomButton(Context context) {
        super(context);
        init();
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setTextColor(Color.WHITE);
        setBackgroundResource(R.color.colorAccent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            TypedValue outValue = new TypedValue();
            getContext().getTheme()
                    .resolveAttribute(R.attr.selectableItemBackgroundBorderless, outValue, true);
            setForeground(getContext().getDrawable(outValue.resourceId));
        }
    }
}
