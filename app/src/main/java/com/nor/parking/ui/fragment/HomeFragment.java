package com.nor.parking.ui.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.nor.parking.R;
import com.nor.parking.databinding.UiHomeBinding;
import com.nor.parking.ui.activities.MainActivity;
import com.nor.parking.ui.activities.ParkingLocationActivity;
import com.nor.parking.ui.dialog.OrderDialog;

public class HomeFragment extends BaseFragment<UiHomeBinding> implements View.OnClickListener {
    @Override
    protected int getLayoutId() {
        return R.layout.ui_home;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding.btnLocation.setOnClickListener(this);
        binding.btnOrder.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_location:
                Intent intent = new Intent(getContext(), ParkingLocationActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_order:
                OrderDialog dialog = new OrderDialog(getContext());
                dialog.show();
                dialog.setOnDismissListener(dialog1 -> ((MainActivity) getActivity()).displayInfo());
                break;
        }
    }
}
