package com.nor.parking.ui.fragment;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.nor.parking.App;
import com.nor.parking.R;
import com.nor.parking.databinding.UiAboutBinding;
import com.nor.parking.model.User;

import java.util.Objects;

public class AboutFragment extends BaseFragment<UiAboutBinding> implements View.OnClickListener {
    @Override
    protected int getLayoutId() {
        return R.layout.ui_about;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding.imFacebook.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        startActivity(newFacebookIntent("https://www.facebook.com/duyromeo234"));
    }

    public Intent newFacebookIntent(String url) {
        Uri uri = Uri.parse(url);
        try {
            PackageManager pm = Objects.requireNonNull(getContext()).getPackageManager();
            ApplicationInfo applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0);
            if (applicationInfo.enabled) {
                uri = Uri.parse("fb://facewebmodal/f?href=" + url);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new Intent(Intent.ACTION_VIEW, uri);
    }

}
