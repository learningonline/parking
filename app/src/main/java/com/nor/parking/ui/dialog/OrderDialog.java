package com.nor.parking.ui.dialog;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.nor.parking.App;
import com.nor.parking.dao.JDBCController;
import com.nor.parking.dao.SqlExecute;
import com.nor.parking.databinding.UiOrderBinding;
import com.nor.parking.model.Order;
import com.nor.parking.model.ParkingLocation;
import com.nor.parking.model.User;
import com.nor.parking.ui.activities.MainActivity;
import com.nor.parking.utils.DateFormatUtils;

import java.util.ArrayList;
import java.util.Calendar;

public class OrderDialog extends Dialog implements View.OnClickListener {
    private UiOrderBinding binding;
    private Order order = new Order();
    public OrderDialog(@NonNull Context context) {
        super(context, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar_MinWidth);
        binding = UiOrderBinding.inflate(LayoutInflater.from(context));
        setContentView(binding.getRoot());
        setCanceledOnTouchOutside(true);
        ArrayList<ParkingLocation> data = JDBCController.getData(ParkingLocation.class, SqlExecute.getSqlListParkingLocation());
        ArrayAdapter<ParkingLocation> adapter = new ArrayAdapter(context, android.R.layout.simple_list_item_1, data);
        binding.spParkingLocation.setAdapter(adapter);
        binding.tvTimeIn.setOnClickListener(this);
        binding.tvTimeOut.setOnClickListener(this);
        binding.btnSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == binding.btnSave) {
            if (order.getTimeOut() <= order.getTimeIn()) {
                Toast.makeText(getContext(), "Thời gian ra phải sau thời gian vào", Toast.LENGTH_SHORT).show();
                return;
            }
            int fee = 10000;
            User user = ((App) getContext().getApplicationContext()).getUser();
            order.setUserName(user.getName());
            order.setIdLocation(((ParkingLocation) binding.spParkingLocation.getSelectedItem()).getId());
            ArrayList<Order> orders = JDBCController.getData(Order.class, SqlExecute.getSqlCheckLocationAvailable(order));
            if (orders.isEmpty()) {
                boolean results = JDBCController.execute(SqlExecute.getSqlOrder(order));
                if (results) {
                    long diff = order.getTimeOut() - order.getTimeIn();
                    float hour = diff / 1000f / 60f / 60f;
                    hour = (float) Math.ceil(hour);
                    fee *= hour;
                    if (user.getBalance() < fee) {
                        Toast.makeText(getContext(), "Không đủ tiền", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    user.setBalance(user.getBalance() - fee);
                    JDBCController.execute(SqlExecute.getSqlUpdateBalance(user.getName(), fee));
                    Toast.makeText(getContext(), "Đặt chỗ thành công", Toast.LENGTH_SHORT).show();
                    dismiss();
                } else {
                    Toast.makeText(getContext(), "Đặt chỗ thất bại", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getContext(), "Khung giờ đã được đặt, vui lòng đặt lại", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        Calendar calendar = Calendar.getInstance();
        TimePickerDialog timePicker = new TimePickerDialog(getContext(), (view, hourOfDay, minute) -> {
            calendar.set(Calendar.HOUR, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);
            if (v == binding.tvTimeIn) {
                order.setTimeIn(calendar.getTimeInMillis());
                binding.tvTimeIn.setText(DateFormatUtils.formatTime(calendar.getTimeInMillis()));
            } else {
                order.setTimeOut(calendar.getTimeInMillis());
                binding.tvTimeOut.setText(DateFormatUtils.formatTime(calendar.getTimeInMillis()));
            }
        }, calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), true);
        timePicker.show();
    }
}
