package com.nor.parking.ui.activities;

import android.os.Bundle;

import com.nor.parking.R;
import com.nor.parking.adapter.ParkingLocationAdapter;
import com.nor.parking.dao.JDBCController;
import com.nor.parking.dao.SqlExecute;
import com.nor.parking.databinding.ActivityParkingLocationBinding;
import com.nor.parking.model.Order;
import com.nor.parking.model.ParkingLocation;
import com.nor.parking.ui.dialog.ListOrderDialog;

import java.util.ArrayList;

public class ParkingLocationActivity extends BaseActivity<ActivityParkingLocationBinding> implements ParkingLocationAdapter.OnItemLocationListener {

    private ParkingLocationAdapter adapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_parking_location;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ParkingLocationAdapter(getLayoutInflater());
        adapter.setListener(this);
        binding.lvLocation.setAdapter(adapter);
        binding.refresh.setOnRefreshListener(this::loadData);
        loadData();
    }

    private void loadData() {
        ArrayList<ParkingLocation> data = JDBCController.getData(ParkingLocation.class, SqlExecute.getSqlListParkingLocation());
        for (ParkingLocation location : data) {
            ArrayList<Order> orders = JDBCController.getData(Order.class, SqlExecute.getSqlListOrderParkingLocation(location.getId()));
            location.setOrders(orders);
        }
        adapter.setData(data);
        binding.refresh.setRefreshing(false);
        int available = 0;
        for (ParkingLocation item : data) {
            if (!(item.getOrders().size() > 0 && item.getOrders().get(0).getTimeIn() <= System.currentTimeMillis())) {
                available++;
            }
        }
        binding.tvAvailable.setText(available + "/" + data.size());
    }

    @Override
    public void onItemLocationClicked(ParkingLocation item) {
        ListOrderDialog dialog = new ListOrderDialog(this, item.getOrders());
        dialog.show();
    }
}
