package com.nor.parking.ui.dialog;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.nor.parking.App;
import com.nor.parking.adapter.OrderAdapter;
import com.nor.parking.dao.JDBCController;
import com.nor.parking.dao.SqlExecute;
import com.nor.parking.databinding.UiListOrderBinding;
import com.nor.parking.databinding.UiOrderBinding;
import com.nor.parking.model.Order;
import com.nor.parking.model.ParkingLocation;
import com.nor.parking.utils.DateFormatUtils;

import java.util.ArrayList;
import java.util.Calendar;

public class ListOrderDialog extends Dialog {
    private UiListOrderBinding binding;
    private OrderAdapter adapter;

    public ListOrderDialog(@NonNull Context context, ArrayList<Order> orders) {
        super(context, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar_MinWidth);
        binding = UiListOrderBinding.inflate(LayoutInflater.from(context));
        setContentView(binding.getRoot());
        setCanceledOnTouchOutside(true);
        adapter = new OrderAdapter(LayoutInflater.from(context));
        adapter.setData(orders);
        binding.lvOrders.setAdapter(adapter);
    }

}
