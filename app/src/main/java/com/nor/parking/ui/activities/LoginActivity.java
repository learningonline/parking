package com.nor.parking.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.nor.parking.App;
import com.nor.parking.R;
import com.nor.parking.dao.JDBCController;
import com.nor.parking.dao.SqlExecute;
import com.nor.parking.databinding.ActivityLoginBinding;
import com.nor.parking.model.User;

import java.util.ArrayList;

public class LoginActivity extends BaseActivity<ActivityLoginBinding> implements View.OnClickListener {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Login");
        String json = sharedPreferences.getString(User.class.getName(), "");
        if (!json.isEmpty()) {
            User user = new Gson().fromJson(json, User.class);
            login(user);
            return;
        }
        binding.btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String userName = binding.edtUserName.getText().toString();
        String password = binding.editPassword.getText().toString();
        if (userName.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "User name or password is empty", Toast.LENGTH_SHORT).show();
            return;
        }
        ArrayList<User> users = JDBCController.getData(User.class, SqlExecute.getSqlLogin(userName, password));
        if (users.size() == 0) {
            Toast.makeText(this, "User name or password incorrect", Toast.LENGTH_SHORT).show();
            return;
        }
        sharedPreferences.edit().putString(User.class.getName(), new Gson().toJson(users.get(0))).apply();
        login(users.get(0));
    }

    private void login(User user) {
        ((App) getApplicationContext()).setUser(user);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
