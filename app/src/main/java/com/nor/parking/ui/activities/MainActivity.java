package com.nor.parking.ui.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.nor.parking.App;
import com.nor.parking.R;
import com.nor.parking.databinding.ActivityMainBinding;
import com.nor.parking.model.User;
import com.nor.parking.ui.fragment.AboutFragment;
import com.nor.parking.ui.fragment.HomeFragment;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements BottomNavigationView.OnNavigationItemSelectedListener {

    private AboutFragment fmProfile = new AboutFragment();
    private HomeFragment fmHome = new HomeFragment();


    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpFragment();
        showFragment(fmHome);
        displayInfo();
        binding.nav.setOnNavigationItemSelectedListener(this);
    }

    public void displayInfo() {
        NumberFormat formatter = new DecimalFormat("#,###");
        User user = ((App) getApplicationContext()).getUser();
        setTitle("Xin chào, " + user.getName() + "    " + formatter.format(user.getBalance()) + "đ");
    }

    private void setUpFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.container, fmProfile);
        transaction.add(R.id.container, fmHome);
        transaction.commit();
    }

    private void showFragment(Fragment fmShow) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        transaction.hide(fmProfile);
        transaction.hide(fmHome);
        transaction.show(fmShow);
        transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_logout, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        sharedPreferences.edit().clear().apply();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tab_home:
                showFragment(fmHome);
                break;
            case R.id.tab_about:
                showFragment(fmProfile);
                break;
        }
        return true;
    }
}
