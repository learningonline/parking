package com.nor.parking.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.nor.parking.R;
import com.nor.parking.databinding.ItemLocationBinding;
import com.nor.parking.model.ParkingLocation;

import java.util.ArrayList;

public class ParkingLocationAdapter extends RecyclerView.Adapter<ParkingLocationAdapter.ParkingLocationViewHolder> {

    private ArrayList<ParkingLocation> data;
    private LayoutInflater inflater;
    private OnItemLocationListener listener;

    public ParkingLocationAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setListener(OnItemLocationListener listener) {
        this.listener = listener;
    }

    public void setData(ArrayList<ParkingLocation> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ParkingLocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ParkingLocationViewHolder(ItemLocationBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(ParkingLocationAdapter.ParkingLocationViewHolder holder, int position) {
        holder.bind(data.get(position));
        holder.itemView.setOnClickListener(v -> {
            if (listener != null) {
                listener.onItemLocationClicked(data.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class ParkingLocationViewHolder extends RecyclerView.ViewHolder {
        private ItemLocationBinding binding;
        public ParkingLocationViewHolder(ItemLocationBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void bind(ParkingLocation item) {
            binding.tvName.setText(item.getName());
            if (item.getOrders().size() > 0 && item.getOrders().get(0).getTimeIn() <= System.currentTimeMillis()) {
                binding.tvName.setBackgroundColor(itemView.getContext().getColor(R.color.colorUnAvailable));
            } else {
                binding.tvName.setBackgroundColor(itemView.getContext().getColor(R.color.colorPrimary));
            }
        }
    }

    public interface OnItemLocationListener {
        void onItemLocationClicked(ParkingLocation item);
    }
}
