package com.nor.parking.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.nor.parking.databinding.ItemOrderBinding;
import com.nor.parking.model.Order;
import com.nor.parking.utils.DateFormatUtils;

import java.util.ArrayList;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderViewHolder> {

    private ArrayList<Order> data;
    private LayoutInflater inflater;

    public OrderAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setData(ArrayList<Order> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new OrderViewHolder(ItemOrderBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(OrderAdapter.OrderViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class OrderViewHolder extends RecyclerView.ViewHolder {
        private ItemOrderBinding binding;
        public OrderViewHolder(ItemOrderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void bind(Order item) {
            binding.tvTime.setText(DateFormatUtils.formatDateTime(item.getTimeIn()) + " ~ " + DateFormatUtils.formatDateTime(item.getTimeOut()));
        }
    }
}
